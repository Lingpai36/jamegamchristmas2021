using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    
    public void Play()
    {
            
        SceneManager.LoadScene("Game");

    }
    
    public void Menu()
    {

        SceneManager.LoadScene("Menu");

    }    

    public void Quit()
    {

        Application.Quit();
        Debug.Log("QUIT");

    }

}
