using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(CharacterMotor))]

public class PlayerController : MonoBehaviour
{

    public static PlayerController player;
    public string xAxisLeft = "Horizontal";
    public string yAxisLeft = "Vertical";
    
    CharacterMotor motor;
    ProjectileLauncher gun;
    LookScript aim;
    
    
    void Awake ()
    {

        player = this;
        motor = GetComponent<CharacterMotor>();
        gun = GetComponentInChildren<ProjectileLauncher>();
        aim = GetComponentInChildren<LookScript>();

    }

    
    void Update()
    {

        float x = Input.GetAxis(xAxisLeft);
        float z = Input.GetAxis(yAxisLeft);

        if (transform.position.x >= 171)
        {
        
            if (transform.position.z >= 4375)
            {
         
                z = 1;
                x = 0;
        
            }
            else if (transform.position.z <= -155)
            {
            
                z = -1;
                x = 0; 
        
            }
            else 
            
            x = 1;
        
        }
        else if (transform.position.x <= -171)
        {

            if (transform.position.z >= 4375)
            {
         
                z = 1;
                x = -1;
        
            }
            else if (transform.position.z <= -155)
            {
            
                z = -1;
                x = -1; 
        
            }
            else
            
            x = -1;

        }

        if (transform.position.z >= 4375)
        {
         
            z = 1;
        
        }
        else if (transform.position.z <= -155)
        {
            
            z = -1;
        
        }

        motor.moveDir = transform.right * x + transform.forward * z; 

        if (Input.GetMouseButton(0))
        {

            gun.Shoot(aim.transform.forward);

        }

    }

    public void Death()
    {

        SceneManager.LoadScene("Menu");

    }



    // if (transform.position.x > 171)
    //     {
         
    //         x = 1;
        
    //     }
    //     else if (transform.position.x < -171)
    //     {
            
    //         x = -1;
        
    //     }

    //     if (transform.position.z > 4375)
    //     {
         
    //         z = 1;
        
    //     }
    //     else if (transform.position.z < -155)
    //     {
            
    //         z = -1;
        
    //     }





    
    

}
