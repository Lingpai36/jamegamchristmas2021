using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public Text elvesRemaining;
    public Text centerBox;

    public GameObject hat;

    void Awake()
    {

        hat.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {

       if(GameObject.FindGameObjectsWithTag("Enemy").Length == 37)
       {    
                    
            hat.SetActive(true);

       }

       elvesRemaining.text = "Remaining: " + GameObject.FindGameObjectsWithTag("Enemy").Length.ToString();

       if(Input.GetButtonDown("Cancel"))
       {

           Cursor.lockState = CursorLockMode.None;

           SceneManager.LoadScene("Menu");

       }

    }

    public void Win()
    {

        centerBox.text = "YOU SAVED CHRISTMAS! YAY!" + "\nPress the Esc key to return to the Main Menu.";

    }

}
