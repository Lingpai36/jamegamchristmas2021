using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class CharacterMotor : MonoBehaviour
{
    public Vector3 moveDir;
    public float reducedSpeed = 10;
    public float turnSpeed = 10;
    public float maxVelocityChange = 1;
    Rigidbody body;
    
    void Awake ()
    {

        body = GetComponent<Rigidbody>();

    }

    
    void Update()
    {

        transform.position += moveDir / 10;

        Debug.Log(moveDir);

    }

    // void FixedUpdate ()
    // {

    //     Vector3 velocityChange = moveDir * reducedSpeed - body.velocity;
    //     velocityChange.y = 0;

    //     if (velocityChange.magnitude > maxVelocityChange)
    //     {

    //         velocityChange = velocityChange.normalized * maxVelocityChange;

    //     }

    //     body.AddForce(velocityChange, ForceMode.VelocityChange);

    // }


}
